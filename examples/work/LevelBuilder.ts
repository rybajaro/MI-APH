import Scene from '../../ts/engine/Scene';
import { PIXICmp } from '../../ts/engine/PIXIObject';
import PIXIObjectBuilder from '../../ts/engine/PIXIObjectBuilder';
import { TAG_PLAYER, TILE_SIZE, TAG_WALL, TAG_OBSTACLE, ATTR_MODEL, TAG_BOMB, ATTR_BUILDER, TAG_DOOR, TAG_EXPLOSION, EXPLOSION_TIME, ATTR_SCORE, SCORE_OBSTACLE_DESTRUCTION, TAG_ENEMY, TAG_NOTHING, TAG_BONUS_BOMBS, TAG_BONUS_SCORE, TAG_BONUS, TAG_BONUS_RANGE, ATTR_POSITION} from './constants';
import { BombermanKeyboardController, MoveComponent, BombTimerComponent, TimerComponent, ExploderComponent, DropperComponent, EnemyAIComponent, LevelingComponent, PlayerHitterComponent, StatusComponent, ScoringComponent, BonusBombsComponent, BonusScoreComponent, BonusRangeComponent, MenuKeyboardController } from './Component';
import { KeyInputComponent } from '../../ts/components/KeyInputComponent';
import { Model } from './Model';
import Vec2 from '../../ts/utils/Vec2';
import { Texture} from 'pixi.js';
import { Utils, textureFrame } from './Utils';
import { PixiRunner } from '../../ts/PixiRunner';

export class LevelBuilder {
    model: Model;
    engine: PixiRunner;

    playerTexture: PIXI.Texture;
    wallTexture: PIXI.Texture;
    obstacleTexture: PIXI.Texture;
    bombTexture: PIXI.Texture;
    doorTexture: PIXI.Texture;
    explosionTexture: PIXI.Texture;
    enemyTexture: PIXI.Texture;
    bonusScoreTexture: PIXI.Texture;
    bonusBombsTexture: PIXI.Texture;
    bonusRangeTexture: PIXI.Texture;

    scenes:Scene[] = [];


    constructor(model: Model){
        this.model = model;
    }


    private createPlayer(x: number, y: number, scene: Scene){
        let obj:PIXICmp.Sprite = this.createSprite(x, y, scene, TAG_PLAYER);
        obj.name = TAG_PLAYER;
        obj.addComponent(new BombermanKeyboardController());
        obj.addComponent(new MoveComponent());
        obj.addComponent(new PlayerHitterComponent());
        obj.addComponent(new ExploderComponent());
        return obj;
    }

    private createWall(x: number, y: number, scene: Scene){
        let obj = this.createSprite(x, y, scene, TAG_WALL);
        return obj;
    }

    private createObstacle(x: number, y: number, scene: Scene){
        let obj = this.createSprite(x, y, scene, TAG_OBSTACLE);
        obj.addComponent(new ExploderComponent());
        return obj;
    }
    
    private createBomb(x: number, y: number, scene: Scene){
        let obj = this.createSprite(x, y, scene, TAG_BOMB);
        obj.addComponent(new BombTimerComponent(this.model.bomb_time));
        return obj;
    }

    private createDoor(x: number, y: number, scene: Scene){
        return this.createSprite(x, y, scene, TAG_DOOR);
    }

    private createExplosion(x: number, y: number, scene: Scene){
        let obj = this.createSprite(x, y, scene, TAG_EXPLOSION);
        obj.addComponent(new TimerComponent(EXPLOSION_TIME));
        return obj;
    }

    private createEnemy(x: number, y: number, scene: Scene){
        let obj = this.createSprite(x, y, scene, TAG_ENEMY);
        obj.addComponent(new ExploderComponent());
        obj.addComponent(new MoveComponent());
        obj.addComponent(new EnemyAIComponent());
        return obj;
    }

    private createText(x: number, y: number, scene: Scene, text:string){
        let pos = Utils.modelToGameCoord(new Vec2(x, y));
        let obj = new PIXICmp.Text("",text);
        obj.style = new PIXI.TextStyle({
            fill :"#ffffff",
            fontStyle : "bold",
            fontSize: "20pt"
        })
        obj.x = pos.x;
        obj.y = pos.y;
        scene.stage.getPixiObj().addChild(obj);
        obj.addComponent(new StatusComponent());
        return obj;
    }

    private createBonusBombs(x: number, y: number, scene: Scene){
        let obj = this.createSprite(x, y, scene, TAG_BONUS_SCORE);
        obj.addComponent(new BonusBombsComponent());
        return obj;
    }

    private createBonusScore(x: number, y: number, scene: Scene){
        let obj = this.createSprite(x, y, scene, TAG_BONUS_BOMBS);
        obj.addComponent(new BonusScoreComponent());
        return obj;
    }

    private createBonusRange(x: number, y: number, scene: Scene){
        let obj = this.createSprite(x, y, scene, TAG_BONUS_RANGE);
        obj.addComponent(new BonusRangeComponent());
        return obj;
    }

    /**
     * Create object, add it's atributes and components and add it to scene
     * 
     * @param x model coordinates (tile) position
     * @param y model coordinates (tile) position
     * @param scene 
     * @param type type (number) of created object (number corresponding to tag)
     * @returns created sprite
     */
    createObject(x: number, y: number, scene: Scene, type: number) {
        let spr: PIXICmp.Sprite = null;
        if (type >= 128) {
            spr = this.createBomb(x, y, scene);
            type -= 128;
        }
        let tag: string = Utils.numberToTag(type);
        switch (tag) {
            case TAG_PLAYER: {
                spr = this.createPlayer(x, y, scene);
                break;
            }
            case TAG_NOTHING: {
                break;
            }
            case TAG_WALL: {
                spr = this.createWall(x, y, scene);
                break;
            }
            case TAG_OBSTACLE: {
                spr = this.createObstacle(x, y, scene);
                break;
            }
            case TAG_BOMB: {//to be sure
                spr = this.createBomb(x, y, scene);
                break;
            }
            case TAG_DOOR: {
                spr = this.createDoor(x, y, scene);
                break;
            }
            case TAG_EXPLOSION: {
                spr = this.createExplosion(x, y, scene);
                break;
            }
            case TAG_ENEMY: {
                spr = this.createEnemy(x, y, scene);
                break;
            }
            case TAG_BONUS_BOMBS: {
                spr = this.createBonusBombs(x, y, scene);
                break;
            }
            case TAG_BONUS_SCORE: {
                spr = this.createBonusScore(x, y, scene);
                break;
            }
            case TAG_BONUS_RANGE: {
                spr = this.createBonusRange(x, y, scene);
                break;
            }
        }
        if(spr != null){
            spr.addAttribute(ATTR_POSITION, new Vec2(x,y));
        }
        return spr;
    }

    /**
     * Inner function for creating sprites and assigning 
     * texture to them.
     * 
     * @param x model coordinates (tile) position
     * @param y gmodel coordinates (tile) position
     * @param scene 
     * @param tag tag of created object
     * @returns created sprite
     */
    private createSprite(x: number, y: number, scene: Scene, tag: string) {
        let pos = Utils.modelToGameCoord(new Vec2(x, y));
        var texture: Texture;
        switch (tag) {
            case TAG_PLAYER: {
                texture = this.playerTexture;
                break;
            }
            case TAG_BOMB: {
                texture = this.bombTexture;
                break;
            }
            case TAG_OBSTACLE: {
                texture = this.obstacleTexture;
                break;
            }
            case TAG_WALL: {
                texture = this.wallTexture;
                break;
            }
            case TAG_DOOR: {
                texture = this.doorTexture;
                break;
            }
            case TAG_EXPLOSION: {
                texture = this.explosionTexture;
                break;
            }
            case TAG_ENEMY: {
                texture = this.enemyTexture;
                break;
            }
            case TAG_BONUS_SCORE: {
                texture = this.bonusScoreTexture;
                break;
            }
            case TAG_BONUS_BOMBS: {
                texture = this.bonusBombsTexture;
                break;
            }
            case TAG_BONUS_RANGE: {
                texture = this.bonusRangeTexture;
                break;
            }
        }
        let obj = new PIXICmp.Sprite(tag, texture);

        new PIXIObjectBuilder(scene).localPos(pos.x, pos.y).anchor(0.5, 0.5)
            .scale(TILE_SIZE / obj.width, TILE_SIZE / obj.height).build(obj);

        scene.stage.getPixiObj().addChild(obj);
        return obj;
    }

    /**
     * Create whole map from model
     * @param scene 
     */
    private createMap(scene: Scene){
        let obstacles:PIXICmp.Sprite[] = [];
        let map = this.model.mapDat;
        for(let i = 0; i < this.model.height; i++){
            for(let j = 0; j < this.model.width ; j++){
                let spr:PIXICmp.Sprite = this.createObject(j,i,scene, map[i][j]);
                if(spr != null && spr.getTag() == TAG_OBSTACLE)
                    obstacles.push(spr);
            }
        }

        for(let i = 0 ; i < this.model.drops.length; i++){
            let elem = this.model.drops[i];
            if(obstacles.length == 0){
                throw "too few obstacles for drops"
            }  
            let pos: number = Math.floor(Math.random()*obstacles.length);
            obstacles[pos].addComponent(new DropperComponent(elem));
            obstacles.splice(pos, 1);
        }
    }

    /**
     * Function for preloading textures
     */
    private loadTextures(){
        this.playerTexture = PIXI.Texture.fromImage(TAG_PLAYER);
        this.wallTexture = PIXI.Texture.fromImage(TAG_WALL);
        this.obstacleTexture = PIXI.Texture.fromImage(TAG_OBSTACLE);
        this.bombTexture = PIXI.Texture.fromImage(TAG_BOMB);
        this.doorTexture = PIXI.Texture.fromImage(TAG_DOOR);
        this.explosionTexture = PIXI.Texture.fromImage(TAG_EXPLOSION);
        this.enemyTexture = PIXI.Texture.fromImage(TAG_ENEMY);

        let bt = PIXI.BaseTexture.fromImage(TAG_BONUS);
        this.bonusScoreTexture = new PIXI.Texture(bt);
        this.bonusScoreTexture.frame = textureFrame(24,0,1);

        this.bonusBombsTexture = new PIXI.Texture(bt);
        this.bonusBombsTexture.frame = textureFrame(24,0,2);

        this.bonusRangeTexture = new PIXI.Texture(bt);
        let rec = textureFrame(24,2,2);
        this.bonusRangeTexture.frame = textureFrame(24,2,2);
    }

    /**
     * Handles transition to next level
     * @param scene 
     */
    nextLevel(){
        if(!this.model.hasNextLevel()){
            return false;
        }
        this.resetLevel(this.model.currentLevel+1);
        return true;
    }

    /**
     * Handles whole restart of level (after dying)
     * @param scene 
     */
    restartLevel(){
        this.resetLevel(this.model.currentLevel);
        return true;   
    }

    /**
     * 
     * @param switchTo name of scane to which switch
     */
    private switchScene(switchTo:string){
        let scene:Scene = this.scenes[switchTo];
        if(scene == null){
            throw "WRONG SCENE NAME: " + switchTo;
        }
        this.engine.scene = scene;
        this.engine.app.stage = scene.stage.getPixiObj();
        return scene;
    }

    /**
     * Stops game and shows STOP scene
     */
    stop(){
        //this.clear(this.engine.scene);
        this.switchScene("pause");
        console.log("PAUSED");
    }

    /**
     * Resumes game by changing back to game scene from different scene
     */
    resume(){
        this.switchScene("game");
        console.log("RESUMED");
    }

    /**
     * Handles transition after wining game
     */
    win(){      
        let score = this.model.score;
        this.reset();
        let scene:Scene = this.switchScene("win");
        (<PIXI.Text>scene.stage.getPixiObj().getChildAt(0)).text = "YOU WIN\nscore:"+score+"\nplay again by U";
        console.log("WIN");
    }

    /**
     * Handles transition after losing game
     */
    lose(){        
        let score = this.model.score;
        this.reset();
        let scene:Scene = this.switchScene("lose");
        (<PIXI.Text>scene.stage.getPixiObj().getChildAt(0)).text = "YOU LOSE\nscore:"+score+"\nU to play again"

        console.log("LOSE");
    }

    /**
     * Resets whole game including lives and score
     */
    private reset(){
        this.resetLevel(0);
        this.model.score = 0;
        this.model.lives = 3;
    }

    /**
     * Resets scene and switches (resets) model to given level
     * @param level index of level to switch to
     */
    private resetLevel(level:number){
        this.engine.scene.clearScene();
        let scene = new Scene(this.engine.app);
        this.engine.scene = scene;
        this.model.setLevel(level);
        scene.addGlobalAttribute(ATTR_MODEL, this.model);
        scene.addGlobalAttribute(ATTR_BUILDER, this);
        scene.addGlobalComponent(new LevelingComponent());
        scene.addGlobalComponent(new KeyInputComponent());
        scene.addGlobalComponent(new ScoringComponent());
        this.createMap(scene);
        this.createText(this.model.width, 0, scene, ""); 
        this.scenes["game"] = scene;
        return scene; 
    }

    private createTextSceene(text:string, textColour:string = "#ffffff"){
        let scene = new Scene(this.engine.app);
        scene.addGlobalComponent(new KeyInputComponent());
        scene.addGlobalComponent(new MenuKeyboardController());
        scene.addGlobalAttribute(ATTR_BUILDER, this);      

        let obj = new PIXICmp.Text("",text);
        obj.style = new PIXI.TextStyle({
            fill :textColour,
            fontStyle : "bold",
            fontSize: "40pt"
        })
        obj.x = 50;
        obj.y = 50;
        scene.stage.getPixiObj().addChild(obj);
        return scene;

    }

    /**
     * Prepare and save special scenes
     */
    private createScenes(){
        this.scenes["pause"] = this.createTextSceene("PAUSED\nunpause by U");
        this.scenes["win"] = this.createTextSceene("WIN","#00FF00");
        this.scenes["lose"] = this.createTextSceene("LOSE","#FF0000");
    }

    init(engine: PixiRunner) {
        this.engine = engine;
        this.createScenes();
        this.loadTextures();
        this.resetLevel(0);
    }
}