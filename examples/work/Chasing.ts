import { Model } from "./Model";
import Vec2 from "../../ts/utils/Vec2";
import { DIRECTIONS } from "./Utils";
import PriorityQueue from "../../ts/utils/datastruct/PriorityQueue";

/**
 * Supporting class for pathfinding algorithms
 */
class Pair<U, V>{
    first: U;
    second: V;
    constructor(first: U, second: V) {
        this.first = first;
        this.second = second;
    }
}

export class Chasing{
    /**
     * 
     * @param position starting position
     * @param model 
     * @returns longest path in random (non-zero length)
     *  direction as array if there is no such path, return []
     */
    static randomDirection(position:Vec2, model:Model){
        let path:Vec2[] = [];
        let possible:Vec2[] = [];
        for (const possibility of DIRECTIONS) {
            let pos:Vec2 = possibility.add(position);
            if(model.mapDat[pos.y][pos.x] == 0)
                possible.push(possibility);
        }
        if(possible.length == 0)
            return [];

        let rand:number = Math.floor(Math.random()*possible.length);
        let curr = position.add(possible[rand]);
        while(model.mapDat[curr.y][curr.x] == 0){
            path.push(curr);
            curr = curr.add(possible[rand]);
        }
        return path;
    }

    /**
     * 
     * @param model 
     * @param position starting position
     * @param cleanVal fill free postions by this number, default 0
     * @returns array with [search map, ancestor map(all null), targetPosition]
     */
    private static createSearchMap(model:Model, position:Vec2, cleanVal:number = 0){
        let map:number[][] = [];
        let ancestors:Vec2[][] = [];
        let target:Vec2 = null;
        for (let i = 0; i < model.mapDat.length; i++) {
            map[i] = [];
            ancestors[i] = [];
            for (let j = 0; j < model.mapDat[i].length; j++) {
                ancestors[i][j] = null;
                let val = cleanVal;  
                if(model.mapDat[i][j] != 0)
                    val = -1;
                if(model.mapDat[i][j] %128 == 127){//target
                    val = cleanVal;
                    target = new Vec2(j, i);
                }
                map[i][j]=val;
            }
        }
        map[position.y][position.x] = 0;
        return [map, ancestors, target];
    }

    /**
     * 
     * @param position starting position
     * @param map search map
     * @returns neighbous in all directions that are not unaccesible (-1)
     */
    static getNeighbours(position:Vec2, map:number[][]){
        let neighbours:Vec2[] = [];
        for (const direction of DIRECTIONS) {
            let newPosition = position.add(direction);
            if(map[newPosition.y][newPosition.x] != -1)
                neighbours.push(newPosition);
        }
        return neighbours;
    }

    /**
     * 
     * @param position starting position
     * @param map search map
     * @returns array of neighbours with cost 0
     */
    static getClenNeighbours(position:Vec2, map:number[][]){
        let neighbours:Vec2[] = [];
        for (const direction of DIRECTIONS) {
            let newPosition = position.add(direction);
            if(map[newPosition.y][newPosition.x] == 0)
                neighbours.push(newPosition);
        }
        return neighbours;
    }

    /**
     * Basic breadth first search
     * 
     * @param position starting position
     * @param model 
     * @returns found path as array or []
     */
    static search(position:Vec2, model:Model){
        let space:any[] = this.createSearchMap(model, position);
        let map:number[][] = space[0];
        let ancestors:Vec2[][] = space[1];
        let target:Vec2 = space[2];
        let finished:boolean = false;
        
        let que:Vec2[] = [position];
        while(que.length > 0 && !finished){
            let pos:Vec2 = que.shift();
            for (const neighbor of this.getClenNeighbours(pos, map)) {
                map[neighbor.y][neighbor.x] = map[neighbor.y][neighbor.x]+1;
                ancestors[neighbor.y][neighbor.x] = pos;
                que.push(neighbor);
                if(neighbor.equals(target)){
                    finished = true;
                    break;
                }
            }
        }
        
        let path:Vec2[] = [];
        let current = target;
        if(finished == false){
            return [];
        }
        while(position.x != current.x || position.y != current.y){
            path.push(current);
            current = ancestors[current.y][current.x];
        }
        path = path.reverse();
        return path;
    }

    /**
     * A-Star search
     * 
     * @param position starting position
     * @param model 
     * @returns found path as array
     */
    static aStarSearch(position:Vec2, model:Model){
        let space:any[] = this.createSearchMap(model, position, Number.MAX_SAFE_INTEGER);
        let map:number[][] = space[0];
        let ancestors:Vec2[][] = space[1];
        let target:Vec2 = space[2];
        let finished:boolean = false;   
        
        let closed:boolean[][] = [];
        for(let i:number = 0; i < map.length; i++){
            closed[i]=[];
            for(let j:number = 0; j < map[i].length; j++){
                closed[i][j] = false;
            }
        }
        let que = new PriorityQueue<Pair<Vec2, number>>((item1, item2) => {
            return item2.second - item1.second;
        });

        que.enqueue(new Pair<Vec2, number>(position, 0));
        while(!que.isEmpty() && !finished){
            let item = que.dequeue();
            let pos = item.first;
            closed[pos.y][pos.x] = true;
            if(pos.equals(target)){
                finished = true;
                break;
            }
            for (const neighbor of this.getNeighbours(pos, map)) {
                let nPrice = map[pos.y][pos.x] + 1;
                if(!closed[neighbor.y][neighbor.x] && nPrice < map[neighbor.y][neighbor.x]){
                    map[neighbor.y][neighbor.x] = nPrice;
                    ancestors[neighbor.y][neighbor.x] = pos;
                    let heuristic = neighbor.manhattanDistance(target);
                    que.enqueue(new Pair<Vec2, number>(neighbor, nPrice+heuristic));
                }
            }
        }
        
        let path:Vec2[] = [];
        let current = target;
        if(finished == false){
            return [];
        }
        while(position.x != current.x || position.y != current.y){
            path.push(current);
            current = ancestors[current.y][current.x];
        }
        path = path.reverse();
        return path;
    }
/**
     * 
     * @param position starting position
     * @param map model map
     * @returns random accesible neighbour or null if none
     */
    static getRandomNeighbour(position:Vec2, map:number[][]){
        let neighbours:Vec2[] = [];
        for (const direction of DIRECTIONS) {
            let newPosition = position.add(direction);
            if(map[newPosition.y][newPosition.x] == 0 || map[newPosition.y][newPosition.x]%128 == 127)
                neighbours.push(newPosition);
        }
        if(neighbours.length == 0){
            return null;
        }
        let rand = Math.floor(Math.random() * neighbours.length);
        return neighbours[rand];
    }

    /**
     * Returns randomly generated path through accesible (0, 127) fields
     * 
     * @param position starting position
     * @param model 
     * @param length length of requested path
     * @returns path of length @length or [] if not possible
     */
    static randomPath(position:Vec2, model:Model, length:number){
        let path:Vec2[] = [];
        let current:Vec2 = position;
        while(path.length < length){
           let neighbour:Vec2 = this.getRandomNeighbour(current, model.mapDat);
           if(neighbour == null){
               return path;
           }
            current = neighbour;
            path.push(neighbour);
        }
        return path;
    }
}