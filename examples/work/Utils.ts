import Vec2 from "../../ts/utils/Vec2";
import { TILE_SIZE, TAG_ENEMY, TAG_WALL, TAG_OBSTACLE, TAG_BOMB, TAG_DOOR, TAG_PLAYER, TAG_NOTHING, TAG_BONUS_SCORE, TAG_BONUS_BOMBS, TAG_BONUS_RANGE, ATTR_POSITION, TAG_EXPLOSION } from "./constants";
import Component from "../../ts/engine/Component";
import { Rectangle } from "pixi.js";
import { PIXICmp } from "../../ts/engine/PIXIObject";

export const DIRECTIONS:Vec2[]=[new Vec2(-1,0),new Vec2(1,0),new Vec2(0,-1),new Vec2(0,1)];

export class Utils{    
    
    /**
     * 
     * @param pos position in game coordinates (pixels)
     * @returns position in model coordinates (tiles)
     */
    static modelToGameCoord(pos: Vec2){
        return new Vec2(TILE_SIZE*(pos.x+0.5), TILE_SIZE*(pos.y+0.5));
    }

    /**
     * 
     * @param pos position in model coordinates (tiles)
     * @returns position in game coordinates (pixels)
     */
    static gameToModelCoord(pos: Vec2){
        return new Vec2(Math.round(pos.x/TILE_SIZE-0.5), Math.round(pos.y/TILE_SIZE-0.5));
    }

    /**
     * 
     * @param object ComponentObject to find position for
     * @returns position of object in model coordinates (tiles)
     */
    static getObjectPosition(object: PIXICmp.ComponentObject){
        return <Vec2>object.getAttribute(ATTR_POSITION);
    }

    /**
     * 
     * @param object Component to find position for
     * @returns position of object in model coordinates (tiles)
     */
    static getPosition(object: Component){
        return <Vec2>object.owner.getAttribute(ATTR_POSITION);
    }

    /**
     * transforms tag to corresponding number code
     */
    static tagToNumber(tag: string) {
        if (tag === TAG_NOTHING) return 0;
        if (tag === TAG_WALL) return 1;
        if (tag === TAG_OBSTACLE) return 2;
        if (tag === TAG_DOOR) return 5;
        if (tag === TAG_ENEMY) return 6;
        if (tag === TAG_PLAYER) return 127;
        if (tag === TAG_BOMB) return 128;
        if (tag === TAG_BONUS_SCORE) return 7;
        if (tag === TAG_BONUS_BOMBS) return 8;
        if (tag === TAG_BONUS_RANGE) return 9;
        if (tag == TAG_EXPLOSION) return 126;
        throw "UNKNOWN TAG" + tag;
    }
    /**
     * transforms number code to corresponding tag
     */
    static numberToTag(tag:number){
        if(tag > 128) tag-= 128;
        if (tag == 0) return TAG_NOTHING;
        if (tag == 1) return TAG_WALL;
        if (tag == 2) return TAG_OBSTACLE;
        if (tag == 5) return TAG_DOOR;
        if (tag == 6) return TAG_ENEMY;
        if (tag == 7) return TAG_BONUS_SCORE;
        if (tag == 8) return TAG_BONUS_BOMBS;
        if (tag == 9) return TAG_BONUS_RANGE;
        if (tag == 126) return TAG_EXPLOSION;
        if (tag == 127) return TAG_PLAYER;
        if (tag == 128) return TAG_BOMB;
        throw "UNKNOWN TAG " + tag;
    }
}

/**
 * Function for navigation in texture atlas
 * @param tileSize \size of tile in texture atlas
 * @param x tile position of rectangle
 * @param y tile position of rectangle
 * @returns Rectangle for frame of texture
 */
export function textureFrame(tileSize: number, x:number, y:number){
    return new Rectangle(tileSize*x, tileSize*y, tileSize, tileSize);
}