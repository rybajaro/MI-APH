import { KEY_UP, KEY_RIGHT, KEY_X } from '../../ts/components/KeyInputComponent';
import { ATTR_MODEL, MSG_MOVE, MSG_HIT, MOVE_TIME, TILE_SIZE, MSG_PLANT, ATTR_BUILDER, TAG_PLAYER, MSG_PLANT_UNSUCCESSFUL, TAG_EXPLOSION, ATTR_SCORE, MSG_EXPLOSION, MSG_DESTROYED, TAG_BOMB, TAG_OBSTACLE, TAG_WALL, TAG_DOOR, TAG_NOTHING, TAG_ENEMY, MSG_MOVE_FINISHED, STATE_SMART_CHASING, STATE_RESTING, SMART_CHASING_ROUND, REST_TIME, MSG_LEVEL_WON, MSG_LEVEL_LOST, MSG_WIN, TAG_BONUS_SCORE, MSG_LOSE, STATE_LOST, STATE_CONFUSED, CHASE_DISTANCE, CONFUSED_PATH_LENGTH, EXPLOSION_CONFUSE_RANGE, ATTR_POSITION} from './constants';
import Component from '../../ts/engine/Component';
import { KeyInputComponent, KEY_DOWN, KEY_LEFT } from '../../ts/components/KeyInputComponent';
import Vec2 from '../../ts/utils/Vec2';
import { Model } from './Model';
import Msg from '../../ts/engine/Msg';
import { LevelBuilder } from './LevelBuilder';
import { Container } from 'pixi.js';
import { DIRECTIONS, Utils } from './Utils';
import { Chasing } from './Chasing';
import { PIXICmp } from '../../ts/engine/PIXIObject';

/**
 * Component (global) responsible for scoring actions 
 * 
 */
export class ScoringComponent extends Component{
    model:Model;
    onInit(){
        this.subscribe(MSG_DESTROYED);
        this.subscribe(MSG_LEVEL_LOST);
        this.subscribe(MSG_LEVEL_WON);
        this.subscribe(MSG_PLANT);
        this.subscribe(MSG_PLANT_UNSUCCESSFUL);
        this.model = this.owner.getScene().getGlobalAttribute(ATTR_MODEL);
    }
    onMessage(msg:Msg){
        if(msg.action == MSG_LEVEL_LOST){
            this.model.score -= 100;
        }
        if(msg.action == MSG_LEVEL_WON){
            this.model.score += 500;
        }
        if(msg.action == MSG_PLANT){
            this.model.score -= 5;
        }
        if(msg.action == MSG_PLANT_UNSUCCESSFUL){
            this.model.score += 5;
        }
        if(msg.action == MSG_DESTROYED){
            let tag = msg.data.getTag();
            if(tag == TAG_ENEMY){
                this.model.score +=500;
            }
            if(tag == TAG_OBSTACLE){
                this.model.score +=100;
            }
        }
    }
    onRemove(){
        this.unsubscribe(MSG_DESTROYED);
        this.unsubscribe(MSG_LEVEL_LOST);
        this.unsubscribe(MSG_LEVEL_WON);
        this.unsubscribe(MSG_PLANT);
        this.unsubscribe(MSG_PLANT_UNSUCCESSFUL);
    }
}

/**
 * Component (global) managing changes and reseting of levels 
 * as well as WIN/LOSE conditions
 * 
 */
export class LevelingComponent extends Component{
    to_restart = false;
    to_next = false;
    model:Model;

    onInit(){
        this.subscribe(MSG_LEVEL_WON);
        this.subscribe(MSG_LEVEL_LOST);
        this.subscribe(MSG_DESTROYED);
        this.model = this.owner.getScene().getGlobalAttribute(ATTR_MODEL);
    }

    onUpdate(){
        if(this.to_restart){
            this.to_restart = false;
            let builder: LevelBuilder = this.scene.getGlobalAttribute(ATTR_BUILDER);
            this.model.lives--;
            if(this.model.lives > 0){
                builder.restartLevel();
            }
            else{
                this.sendMessage(MSG_LOSE);
                builder.lose();
            }
        }
        if(this.to_next){
            this.to_next = false;
            let builder: LevelBuilder = this.scene.getGlobalAttribute(ATTR_BUILDER);
            if(!builder.nextLevel()){
                this.sendMessage(MSG_WIN);
                builder.win();
            }
        }
    }

    onMessage(msg:Msg){
        if(msg.action == MSG_DESTROYED && msg.data.getTag() == TAG_PLAYER){
            this.to_restart = true;
        }
        if(msg.action == MSG_LEVEL_LOST){
            this.to_restart = true;
        }
        if(msg.action == MSG_LEVEL_WON){
            this.to_next = true;
        }
    }

    onRemove(){
        this.unsubscribe(MSG_LEVEL_WON);
        this.unsubscribe(MSG_LEVEL_LOST);
        this.unsubscribe(MSG_DESTROYED);
    }
}

/**
 * Component responsible for updating status text
 * shows contains level, score, max # bombs, #bombs, lives
 */
export class StatusComponent extends Component{
    model:Model;
    onInit(){
        this.model = this.owner.getScene().getGlobalAttribute(ATTR_MODEL);
    }

    onUpdate(){
        let score = this.model.score;
        let level = this.model.currentLevel;
        let bombs = this.model.bombs;
        let lives = this.model.lives;
        let max_bombs = this.model.max_bombs;
        let text = "Level: " + level + "\nLives: "+lives+"\nScore: " + score + "\nBombs:" + bombs+"/"+max_bombs;
        (<PIXICmp.Text>this.owner.getPixiObj()).text = text;
    }

}

/**
 * Basic component for managing colisions
 * colision with this object is recieved and then given 
 * to hitRaction method to solve
 * 
 */
export class HitterComponent extends Component{
    model:Model;
    onInit(){
        this.subscribe(MSG_HIT);
        this.model = this.owner.getScene().getGlobalAttribute(ATTR_MODEL);
    }

    /**
     * Method for reactin on colisions. Default does nothing, should be overriden
     * 
     * @param target type of target with which is coliding
     * @param pos current position
     * @param targetPos position of target
     */
    hitReaction(target:number, pos:Vec2, targetPos:Vec2){

    }

    onMessage(msg:Msg){
        let pos = Utils.getPosition(this);
        if(msg.action == MSG_HIT){
            var targetPos:Vec2;
            if(msg.data[0].x == pos.x && msg.data[0].y == pos.y){
                targetPos = msg.data[1];
            }
            else if(msg.data[1].x == pos.x && msg.data[1].y == pos.y){
                targetPos = msg.data[0];
            }
            else return;

            let target:number = this.model.mapDat[targetPos.y][targetPos.x];
            this.hitReaction(target, pos, targetPos);
        }
    }

    onRemove(){
        this.unsubscribe(MSG_HIT);
    }
}

/**
 * Component increasing bomb range on collision
 */
export class BonusRangeComponent extends HitterComponent{
    hitReaction(target:number, pos:Vec2, targetPos:Vec2){
        if(Utils.numberToTag(target) == TAG_PLAYER){
            this.model.explosion_radius += 1;
        }
        
        let builder: LevelBuilder = this.scene.getGlobalAttribute(ATTR_BUILDER);
        this.model.mapDat[pos.y][pos.x] = 0;
        this.owner.remove();       
    }
}

/**
 * Component increasing number(and max number) of bombs on collision
 */
export class BonusBombsComponent extends HitterComponent{
    hitReaction(target:number, pos:Vec2, targetPos:Vec2){
        if(Utils.numberToTag(target) == TAG_PLAYER){
            this.model.max_bombs += 1;
            this.model.bombs += 1;
        }
        this.model.mapDat[pos.y][pos.x] = 0;
        this.owner.remove();
    }
}

/**
 * Component increasing score on collision
 */
export class BonusScoreComponent extends HitterComponent{
    hitReaction(target:number, pos:Vec2, targetPos:Vec2){
        if(Utils.numberToTag(target) == TAG_PLAYER){
            this.model.score += 500;
        }
        this.model.mapDat[pos.y][pos.x] = 0;
        this.owner.remove();
    }
}

/**
 * Component handling special collisions of player (with deadly 
 * objects - enemies, and winning conditions - doors)
 * 
 */
export class PlayerHitterComponent extends HitterComponent{
    hitReaction(target:number, pos:Vec2, targetPos:Vec2){
        if(Model.DEADLY.includes(Utils.numberToTag(target))){
            this.sendMessage(MSG_LEVEL_LOST);
        }
        if(Model.WIN_COND.includes(Utils.numberToTag(target))){
            this.sendMessage(MSG_LEVEL_WON);
        }
    }
}

/**
 * Component handling AI logic of enemy units.
 * Is responsible for deciding path taken and initiating movements.
 */
export class EnemyAIComponent extends Component {
    model: Model;
    state: number;
    timeInState: number;
    path: Vec2[];
    
    onInit() {
        this.subscribe(MSG_MOVE_FINISHED);
        this.subscribe(MSG_DESTROYED);
        this.subscribe(MSG_HIT);
        this.changeState(STATE_CONFUSED);
        this.model = this.owner.getScene().getGlobalAttribute(ATTR_MODEL);
        this.path = [];
    }

    changeState(state: number) {
        this.timeInState = 0;
        this.state = state;
        this.path = [];
    }

    findPath() {
        this.path = Chasing.aStarSearch(Utils.getPosition(this), this.model);
        return this.path.length;
    }

    randomPath(pathLength:number){
        this.path = Chasing.randomPath(Utils.getPosition(this), this.model, pathLength);
        return this.path.length;
    }

    randomDirection() {
        this.path = Chasing.randomDirection(Utils.getPosition(this), this.model);
        return this.path.length;
    }

    requestMove(){
        this.sendMessage(MSG_MOVE_FINISHED, this.owner.getPixiObj());
    }


    onUpdate(delta: number, absolute: number) {
        this.timeInState += delta;
        switch (this.state) {
            case STATE_SMART_CHASING: {
                let player = this.scene.findAllObjectsByTag(TAG_PLAYER)[0]
                if(player == null){
                    console.log("player still not loaded");
                    return;
                }
                let playerPos:Vec2 = Utils.getObjectPosition(player);
                let distance = playerPos.manhattanDistance(Utils.getPosition(this));
                if (this.timeInState > SMART_CHASING_ROUND){
                    this.changeState(STATE_RESTING);
                }
                else if(distance > CHASE_DISTANCE){
                    this.changeState(STATE_LOST);
                }
                else if (this.path.length == 0) { //fresh or finished moving
                    if (this.findPath() == 0) {
                        this.changeState(STATE_LOST);
                    }
                    else
                        this.requestMove();
                }
                break;
            }
            case STATE_RESTING: {
                if (this.timeInState > REST_TIME) {
                    this.changeState(STATE_SMART_CHASING);
                }
                break;
            }
            case STATE_LOST:{
                if (this.path.length == 0) {
                    if(this.timeInState != delta){//path finished
                        this.changeState(STATE_RESTING);
                    }
                    else if (this.randomDirection() == 0) {
                        this.changeState(STATE_RESTING);
                    }
                    else {
                        let rand:number = Math.floor(Math.random()*this.path.length) + 1; 
                        if(rand < this.path.length/2) //twice as likely to make longer paths
                            rand = Math.floor(Math.random()*this.path.length) + 1; 
                        this.path = this.path.slice(0,rand);
                        this.requestMove();
                    }
                }
                break;
            }
            case STATE_CONFUSED:{
                if (this.path.length == 0) {
                    if(delta != this.timeInState){//confused path finished
                        this.changeState(STATE_RESTING);
                    }
                    else if (this.randomPath(CONFUSED_PATH_LENGTH) == 0) {
                        this.changeState(STATE_RESTING);
                    }
                    else 
                        this.requestMove();
                }
                break;
            }
        }
    }

    onMessage(msg: Msg) {
        if (msg.action == MSG_MOVE_FINISHED && msg.data == this.owner.getPixiObj()){
            if(this.state != STATE_RESTING){
                if(this.path.length > 0){
                    let direction = this.path.shift().subtract(Utils.getPosition(this));
                    if(direction.magnitudeSquared() != 1){//last step not finished
                        this.path = [];
                    }
                    else{
                        this.sendMessage(MSG_MOVE, new MovePayload(direction, this.owner.getPixiObj()));
                    }
                }
            }
        }       
         if (msg.action == MSG_HIT && msg.data[0].equals(Utils.getPosition(this))){
            this.changeState(STATE_CONFUSED);
        }
        if(msg.action == MSG_DESTROYED && msg.data.getTag() == TAG_BOMB){
            let pos:Vec2 = Utils.getPosition(this);
            let targetPos:Vec2 = Utils.getObjectPosition(msg.data.getPixiObj());
            let distance:number = pos.manhattanDistance(targetPos);
            if(distance <= EXPLOSION_CONFUSE_RANGE){
                this.changeState(STATE_CONFUSED);
            }
        }
    }

    onRemove(){
        this.unsubscribe(MSG_MOVE_FINISHED);
        this.unsubscribe(MSG_DESTROYED);
        this.unsubscribe(MSG_HIT);
    }
}

/**
 * Component responsible for dropping objects on
 * destruction of it's owner.
 */
export class DropperComponent extends Component {
    drop: string;
    model: Model;
    /**
     * 
     * @param toDrop TAG of object to be dropped on destruction
     */
    constructor(toDrop: string = TAG_NOTHING) {
        super();
        this.drop = toDrop;
    }
    onInit() {
        this.model = this.owner.getScene().getGlobalAttribute(ATTR_MODEL);
    }

    dropIt() {
        let pos = Utils.getPosition(this);
        let builder: LevelBuilder = this.scene.getGlobalAttribute(ATTR_BUILDER);
        builder.createObject(pos.x, pos.y, this.scene, Utils.tagToNumber(this.drop));
        this.model.mapDat[pos.y][pos.x] = Utils.tagToNumber(this.drop);
    }

    onRemove() {
        this.dropIt();
    }
}

/**
 * Component responsible for destroying object when hit by explosion.
 */
export class ExploderComponent extends Component {
    model: Model;

    constructor() {
        super();
    }
    onInit() {
        this.subscribe(MSG_EXPLOSION);
        this.model = this.owner.getScene().getGlobalAttribute(ATTR_MODEL);
    }
    onMessage(msg: Msg) {
        let pos = Utils.getPosition(this);
        if (msg.action == MSG_EXPLOSION && pos.x == msg.data.x && pos.y == msg.data.y && this.owner != null) {
            this.sendMessage(MSG_DESTROYED, this.owner);
            this.model.mapDat[pos.y][pos.x] = 0;
            this.owner.remove();
        }
    }
    onRemove() {
        this.unsubscribe(MSG_EXPLOSION);
    }
}

/**
 * Component responsible for removing object after given amount of time
 */
export class TimerComponent extends Component {
    running: number;
    time_to: number;

    /**
     * 
     * @param time_to time to live
     */
    constructor(time_to: number) {
        super();
        this.time_to = time_to;
    }

    onInit() {
        this.running = 0;
    }

    onUpdate(delta: number, absolute: number) {
        this.running += delta;
        if (this.running > this.time_to) {
            this.owner.remove();
        }
    }
}

/**
 * Component responsible for generating explosion and removing object
 * after given time.
 */
export class BombTimerComponent extends TimerComponent {
    model: Model;
    /**
     * 
     * @param time_to time till explosion
     */
    constructor(time_to: number) {
        super(time_to);
    }
    onInit() {
        super.onInit();
        this.model = this.owner.getScene().getGlobalAttribute(ATTR_MODEL);
    }
    explodeInDirection(direction: Vec2, from: Vec2) {
        for (let dir: Vec2 = direction; dir.magnitude() <= this.model.explosion_radius; dir = dir.add(direction)) {
            let pos = dir.add(from);
            let builder: LevelBuilder = this.scene.getGlobalAttribute(ATTR_BUILDER);
            builder.createObject(pos.x, pos.y, this.scene, Utils.tagToNumber(TAG_EXPLOSION));
            if (this.model.mapDat[pos.y][pos.x] != 0) {
                this.sendMessage(MSG_EXPLOSION, pos);
                break;
            }
        }
    }

    explosion() {
        let pos = Utils.getPosition(this);
        let builder: LevelBuilder = this.scene.getGlobalAttribute(ATTR_BUILDER);
        builder.createObject(pos.x, pos.y, this.scene, Utils.tagToNumber(TAG_EXPLOSION));
        if (this.model.mapDat[pos.y][pos.x] != 0) {
            this.sendMessage(MSG_EXPLOSION, pos);
        }
        for (const direction of DIRECTIONS) {
            this.explodeInDirection(direction, pos);
        }
    }

    onRemove() {
        super.onFinish();
        this.model.bombs++;
        let pos = Utils.getPosition(this);
        this.model.mapDat[pos.y][pos.x] -= 128;
        this.explosion();
    }
}

/**
 * Payload of MSG_MOVE
 */
class MovePayload {
    direction: Vec2;
    who: Container;
    constructor(direction: Vec2, who: Container) {
        this.direction = direction;
        this.who = who;
    }
}

/**
 * Component responsible for interactive actions.
 * In detail it is movement, movement animation, colision detection and planting bombs.
 */
export class MoveComponent extends Component {
    model: Model;
    from: Vec2;
    to: Vec2;
    moving: boolean;
    running: number;
    toPlant:boolean = false;
    onInit() {
        this.subscribe(MSG_MOVE);
        this.subscribe(MSG_PLANT);
        this.model = this.owner.getScene().getGlobalAttribute(ATTR_MODEL);
        this.moving = false;
    }

    onUpdate(delta: number, absolute: number) {
        if (this.moving) {
            this.running += delta;
            if (this.running > MOVE_TIME) {
                this.running = MOVE_TIME;
                this.moving = false;
                this.plantCheck();
                this.sendMessage(MSG_MOVE_FINISHED, this.owner.getPixiObj());
            }
            let progress = TILE_SIZE * (this.running / MOVE_TIME);
            let dir = this.to.subtract(this.from).multiply(progress);
            let pos = Utils.modelToGameCoord(this.from).add(dir);

            this.owner.getPixiObj().x = pos.x;
            this.owner.getPixiObj().y = pos.y;
        }
        else 
            this.plantCheck();
    }

    plantCheck() {
        if (this.toPlant) {
            let pos = Utils.getPosition(this);
            this.toPlant = false;
            if (this.model.mapDat[pos.y][pos.x] < 128 && this.model.bombs > 0) {
                let builder: LevelBuilder = this.scene.getGlobalAttribute(ATTR_BUILDER);
                builder.createObject(pos.x, pos.y, this.scene, Utils.tagToNumber(TAG_BOMB));
                this.model.mapDat[pos.y][pos.x] += 128;
                this.model.bombs--;
            }
            else
                this.sendMessage(MSG_PLANT_UNSUCCESSFUL);
        }
    }

    onMessage(msg: Msg) {
        if (msg.action == MSG_MOVE && !this.moving && msg.data.who == this.owner.getPixiObj()) {
            let dirrection: Vec2 = msg.data.direction;
            let map = this.model.mapDat;
            let from = Utils.getPosition(this);
            let to = from.add(dirrection);
            if (map[to.y][to.x] == 0) {
                let my_tag:number = Utils.tagToNumber(this.owner.getTag());
                map[from.y][from.x] -= my_tag;
                map[to.y][to.x] = my_tag;
                this.owner.addAttribute(ATTR_POSITION, to);
                this.from = from;
                this.to = to;
                this.running = 0;
                this.moving = true;
            }
            else {
                this.sendMessage(MSG_HIT, [ from, to ]);
            }
        }
        if (msg.action == MSG_PLANT && msg.data == this.owner.getPixiObj()) {
            this.toPlant = true;
        }
    }
    onRemove() {
        this.unsubscribe(MSG_MOVE);
        this.unsubscribe(MSG_PLANT);
    }
}

/**
 * ""abstract" Controller for player actions. Contains basic method of interaction. 
 * 
 */
class BombermanController extends Component {

    onUpdate(delta: number, absolute: number) {

    }

    protected goLeft() {
        this.move(new Vec2(-1, 0));
    }

    protected goRight() {
        this.move(new Vec2(1, 0));
    }

    protected goUp() {
        this.move(new Vec2(0, -1));
    }

    protected goDown() {
        this.move(new Vec2(0, 1));
    }

    private move(direction: Vec2) {
        this.sendMessage(MSG_MOVE, new MovePayload(direction, this.owner.getPixiObj()));
    }

    protected plantMomb() {
        this.sendMessage(MSG_PLANT, this.owner.getPixiObj());
    }
}

/**
 * Component for handling keyboard inputs for player sprite
 * is not used globally to allow for future multiplayer extension
 */
export class BombermanKeyboardController extends BombermanController {
    onUpdate(delta: number, absolute: number) {
        let keyComponent = <KeyInputComponent>this.scene.findGlobalComponentByClass(KeyInputComponent.name);
        if(keyComponent == null)
            return;
        if (keyComponent.isKeyPressed(KEY_UP)) {
            this.goUp();
        }
        if (keyComponent.isKeyPressed(KEY_DOWN)) {
            this.goDown();
        }
        if (keyComponent.isKeyPressed(KEY_LEFT)) {
            this.goLeft();
        }
        if (keyComponent.isKeyPressed(KEY_RIGHT)) {
            this.goRight();
        }
        if (keyComponent.isKeyPressed(KEY_X)) {
            this.plantMomb();
        }
        if (keyComponent.isKeyPressed(80)) { //P
            let builder: LevelBuilder = this.scene.getGlobalAttribute(ATTR_BUILDER);
            builder.stop();
        }
    }
}

/**
 * Component for handling keyboard inputs in menu
 */
export class MenuKeyboardController extends Component {
    onUpdate(delta: number, absolute: number) {
        let keyComponent = <KeyInputComponent>this.scene.findGlobalComponentByClass(KeyInputComponent.name);
        if(keyComponent == null)
            return;
        if (keyComponent.isKeyPressed(85)) {//U
            let builder: LevelBuilder = this.scene.getGlobalAttribute(ATTR_BUILDER);
            builder.resume();
        }
    }
}