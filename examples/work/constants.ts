export const TAG_PLAYER = "player";
export const TAG_WALL = "wall";
export const TAG_OBSTACLE = "obstacle";
export const TAG_BOMB = "bomb";
export const TAG_EXPLOSION = "explosion";
export const TAG_DOOR = "door";
export const TAG_NOTHING = "nothing";
export const TAG_ENEMY = "enemy";
export const TAG_JSON = "json";
export const TAG_BONUS = "bonus";
export const TAG_BONUS_BOMBS = "bonus bombs";
export const TAG_BONUS_SCORE = "bonus score";
export const TAG_BONUS_RANGE = "bonus range";


export const ATTR_MODEL = "model";
export const ATTR_BUILDER = "builder";
export const ATTR_SCORE = "score";
export const ATTR_POSITION = "position";


export const MSG_MOVE_FINISHED = "move finished";
export const MSG_MOVE = "move";
export const MSG_HIT = "hit";
export const MSG_PLANT = "plant";
export const MSG_PLANT_UNSUCCESSFUL = "plant unsuccessful";
export const MSG_EXPLOSION = "explosion";
export const MSG_DESTROYED = "destroyed";
export const MSG_LEVEL_WON = "level won";
export const MSG_LEVEL_LOST = "level lost";
export const MSG_GAME_OVER = "game over";
export const MSG_WIN = "win";
export const MSG_LOSE = "lose";


export const EXPLOSION_RADIUS:number = 1;
export const TILE_SIZE:number = 35;
export const MOVE_TIME:number = 500;
export const BOMB_TIME:number = 1500;
export const EXPLOSION_TIME:number = 500;
export const SCORE_OBSTACLE_DESTRUCTION:number = 100;
export const REST_TIME = 500;
export const DEFAULT_LIVES = 3;


export const STATE_SMART_CHASING = 0;
export const STATE_RESTING = 1;
export const STATE_CONFUSED = 2;
export const STATE_LOST = 3;

export const SMART_CHASING_ROUND:number = 1500;
export const CHASE_DISTANCE:number = 4;
export const CONFUSED_PATH_LENGTH:number = 4;
export const EXPLOSION_CONFUSE_RANGE:number = 3;

