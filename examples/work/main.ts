import { PixiRunner } from '../../ts/PixiRunner'
import { LevelBuilder } from './LevelBuilder';
import { Model } from './Model';
import { TAG_WALL, TAG_OBSTACLE, TAG_PLAYER, TAG_BOMB, TAG_DOOR, TAG_EXPLOSION, TAG_ENEMY, TAG_JSON, TAG_BONUS } from './constants';

class Components {    
    engine: PixiRunner;

    // Start a new game
    constructor() {
        this.engine = new PixiRunner();

        this.engine.init(document.getElementById("gameCanvas") as HTMLCanvasElement, 1);

        PIXI.loader
            .reset()    // necessary for hot reload
            .add(TAG_WALL, 'static/semestral/wall.png')
            .add(TAG_OBSTACLE, 'static/semestral/obstacle.png')
            .add(TAG_PLAYER, 'static/semestral/bomberman1.png')
            .add(TAG_BOMB, 'static/semestral/bomb.png')
            .add(TAG_DOOR, 'static/semestral/door.png')
            .add(TAG_EXPLOSION, 'static/semestral/explosion.png')
            .add(TAG_ENEMY, 'static/semestral/enemy.png')
            .add(TAG_JSON, 'static/semestral/Levels.json')
            .add(TAG_BONUS, 'static/semestral/bonus.png')
            .load(() => this.onAssetsLoaded());
    }

    onAssetsLoaded() {
        // load example
        let json = PIXI.loader.resources.json;
        let model = new Model(json);
        new LevelBuilder(model).init(this.engine);
        
    }
}


new Components();

