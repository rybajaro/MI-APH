import { BOMB_TIME, EXPLOSION_RADIUS, TAG_DOOR, TAG_ENEMY, DEFAULT_LIVES } from "./constants";

class Level{
    n:number;
    map: number[][];
    drops:string[];
    constructor(n:number, map:number[][], drops:string[]){
        this.n = n;
        this.map = map;
        this.drops = drops;
    }
}

export class Model{
    static WIN_COND = [TAG_DOOR];
    static DEADLY = [TAG_ENEMY];

    currentLevel:number;
    width:number;
    height:number;
    max_bombs:number;
    bombs:number;
    bomb_time:number;
    explosion_radius:number;
    score: number;
    levels:Level[];
    lives:number;

    /**
     * 1:wall
     * 2:obstacle
     * 128:bomb
     * 5:door
     * 6:enemy
     * 127:player
     * 255:player+bomb
     */
    mapDat:number[][];
    drops:string[] = [TAG_DOOR];

    /**
     * Loads levels from external file
     * @param json input file
     */
    private loadJson(json:any){
        this.levels = [];
        for (const level of json.data.levels) {
            this.levels.push(new Level(level.level, level.mapDat, level.drops));
        }
    }

    /**
     * @returns true if current level isn't last, false othervise
     */
    hasNextLevel(){
        return this.currentLevel < this.levels.length-1;
    }

    /**
     * Sets map to given level. Resets all variables except for score and lives. 
     * This happens even when changing to the same level as current one.
     * @param setTo index of level to which change
     */
    setLevel(setTo: number){
        if(setTo >= this.levels.length){
            return null;
        }
        
        this.loadDefaults();
        this.mapDat = [];
        for (const row of this.levels[setTo].map) {
            this.mapDat.push(row.slice());
        }
        this.height = this.mapDat.length;
        this.width = this.mapDat[0].length;  
        this.drops = this.levels[setTo].drops;
        this.currentLevel = setTo;
        return this;
    }

    private loadDefaults(){
        this.max_bombs = 1;
        this.bombs = this.max_bombs;
        this.bomb_time = BOMB_TIME;
        this.explosion_radius = EXPLOSION_RADIUS;
    }

    /**
     * Initializes model and loads levels from external file
     * @param json input file
     */
    constructor(json:any) {  
        this.loadJson(json);
        this.loadDefaults();
        this.setLevel(0);
        this.currentLevel = 0;
        this.score = 0;
        this.lives = DEFAULT_LIVES;
    }     
}